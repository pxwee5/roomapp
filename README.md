# README #

RoomApp is a room classified app for users to search for long term accommodation.


### Entity Relationship Diagram (ERD) ###
#### User ####
* (String) Name
* (String) User
* (int) Number
* (ArrayList) **Rooms**
* (ArrayList) **Favourites**

#### Rooms ####
* (ArrayList) Images
* (String) Title
* (String) Address
* (ArrayList) Amenities 
* (String) Description
* (Double) Price
* (Object) **Owner (User)**

#### Favourites (Low Priority) ####
* (Object) **Room**
* (Object) **User**

### Markdown Examples ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)